const querystring = require('querystring')
const api = require('./api')
const { convertXMLToJSON, fixParams } = require('./utils')

/*
Properties name was changed for easier understanding (eg. 'u' now is 'user')
Before POST or GET, this properties are back to acceptable parameters for Copyscape

Properties to URL Search, Text Search and Balance (Private Index not included yet)

user          - u -   Your username
apiKey        - k -   Your API key
operation     - o -   API operation
query         - q -   Source URL
comparisons   - c -   Full comparisons
format        - f -   Response format
ignore        - i -   Ignore sites
limit         - l -   Spend limit
example       - x -   Example test
encoding      - e -   Text encoding
text          - t -   Text to be searched
*/


const searchFromURL = async (params) => {
  const {
    user, apiKey, query, example, operation, encoding
  } = params

  const paramsAux = fixParams(params)

  if (!user) return 'User is missing. Use property user.'
  if (!apiKey) return 'API KEY is missing. Use property apiKey.'
  if (!query && !example) return 'Query is missing. Use property query.'

  // define operation and encoding
  if (!operation) paramsAux.o = 'csearch'
  if (!encoding) paramsAux.e = 'UTF-8'

  return api.get('', { params: paramsAux })
    .then(response => convertXMLToJSON(response.data))
}

const searchFromText = async (params) => {
  const {
    user, apiKey, text, example, operation, encoding
  } = params

  const paramsAux = fixParams(params)

  if (!user) return 'User is missing. Use property user.'
  if (!apiKey) return 'API KEY is missing. Use property apiKey.'
  if (!text && !example) return 'Text is missing. Use property text.'

  // define operation and encoding
  if (!operation) paramsAux.o = 'csearch'
  if (!encoding) paramsAux.e = 'UTF-8'

  return api.post('', querystring.stringify(paramsAux))
    .then(response => convertXMLToJSON(response.data))
}

const getBalance = async (params) => {
  const { user, apiKey } = params

  if (!user) return 'User is missing. Use property user.'
  if (!apiKey) return 'API KEY is missing. Use property apiKey.'

  const paramsAux = fixParams(params)

  // define operation
  paramsAux.o = 'balance'

  return api.get('', { params: paramsAux })
    .then(response => convertXMLToJSON(response.data))
}

const copyscape = { searchFromURL, searchFromText, getBalance }

module.exports = copyscape
