const parser = require('fast-xml-parser')
const he = require('he')

const convertXMLToJSON = (xml) => {
  const options = {
    attributeNamePrefix: '@_',
    attrNodeName: 'attr', // default is 'false'
    textNodeName: '#text',
    ignoreAttributes: true,
    ignoreNameSpace: false,
    allowBooleanAttributes: false,
    parseNodeValue: true,
    parseAttributeValue: false,
    trimValues: true,
    cdataTagName: '__cdata', // default is 'false'
    cdataPositionChar: '\\c',
    localeRange: '', // To support non english character in tag/attribute values.
    parseTrueNumberOnly: false,
    attrValueProcessor: a => he.decode(a, { isAttributeValue: true }), // default is a=>a
    tagValueProcessor: a => he.decode(a) // default is a=>a
  };

  if (parser.validate(xml) === true) { // optional (it'll return an object in case it's not valid)
    return parser.parse(xml, options);
  }

  return false
}

const fixParams = (params) => {
  const paramsAux = {}

  const {
    user, apiKey, operation, query, comparisons, format, ignore, limit, example, encoding, text
  } = params

  if (user) { paramsAux.u = params.user }
  if (apiKey) { paramsAux.k = params.apiKey }
  if (operation) { paramsAux.o = params.operation }
  if (query) { paramsAux.q = params.query }
  if (comparisons) { paramsAux.c = params.comparisons }
  if (format) { paramsAux.f = params.format }
  if (ignore) { paramsAux.i = params.ignore }
  if (limit) { paramsAux.l = params.limit }
  if (example) { paramsAux.x = params.example }
  if (encoding) { paramsAux.e = params.encoding }
  if (text) { paramsAux.t = params.text }

  return paramsAux
}

module.exports = {
  convertXMLToJSON,
  fixParams
}
