# Welcome to Copyscape!

This is a library to use [Copyscape](https://www.copyscape.com/) API.

## Installation

This is a  [Node.js](https://nodejs.org/en/)  module available through the  [npm registry](https://www.npmjs.com/).

Before installing,  [download and install Node.js](https://nodejs.org/en/download/). Node.js 0.10 or higher is required.

Installation is done using the  [`npm install`  command](https://docs.npmjs.com/getting-started/installing-npm-packages-locally):

```
$ npm install copyscape --save
```
To use in Node.js

```
const copyscape =  require('copyscape')
```

## Functions

We have just 3 functions so far:

 1. getBalance
 2. searchFromText
 3. searchFromURL

*Note: Every function return a Promise. Default response from API Copyscape was parse from XML to JSON.*


### getBalance

```
copyscape.getBalance(params).then( response  => {
	console.log('balance', response)
})
```

#### Required parameters

 - user
 - apiKey

#### Optional parameters

 - operation (default is 'balance' - just that is allowed to get balance)
 - format (default is xml - html not works yet)

### searchFromText

```
copyscape.searchFromText(params).then( response  => {
	console.log('copies from text', response)
})
```

#### Required parameters

 - user
 - apiKey
 - text

#### Optional parameters

 - encoding (default is 'UTF-8)
 - operation (default is 'csearch' - psearch or cpsearch not works yet)
 - comparisons (default is 0)
 - format (default is xml - html not works yet)
 - ignore (comma-delimited domains to ignore)
 - limit (value in dollars, e.g.  0.50)
 - example (default is none - set to 1 to run tests on API)

### searchFromURL

```
copyscape.searchFromURL(params).then( response  => {
	console.log('copies from text', response)
})
```

#### Required parameters

 - user
 - apiKey
 - query (*urlencoded URL*)

#### Optional parameters

 - operation (default is 'csearch' - psearch or cpsearch not works yet)
 - comparisons (default is 0)
 - format (default is xml - html not works yet)
 - ignore (comma-delimited domains to ignore)
 - limit (value in dollars, e.g.  0.50)
 - example (default is none - set to 1 to run tests on API)

## Test Example

To test if everything is fine, create a **test.js** file:

```
( async () => {

	const copyscape =  require('copyscape')

	// To get user and apiKey: https://www.copyscape.com/apiconfigure.php
	const params = {
		user:  'YOUR-USER',
		apiKey:  'YOUR-API-KEY',
		example:  1
	}

	copyscape.getBalance(params).then( response  => {
		console.log('balance', response)
	})

	copyscape.searchFromText(params).then( response  => {
		console.log('copies from text', response)
	})

	copyscape.searchFromURL(params).then( response  => {
		console.log('copies from url', response)
	})

})()
```

and run file:

```
$ node test
```

## Private Index

Copyscape Private Index allows you to check for duplication and plagiarism **within your own content**, even if it is not online or has not been indexed by search engines.

***This feature it's not working yet, maybe in the future. Be free to help us developing this feature.***

## Contributing

We're looking for co-maintainers! If you want to become a master of time please write to  [Gleidson Henrique](https://github.com/api-spinner).



*Note: This library is maintained individually, not developed by Copyscape.*
