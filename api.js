const axios = require('axios')

module.exports = axios.create({
  baseURL: 'https://www.copyscape.com/api/',
  timeout: 10000,
  headers: { 'content-type': 'application/x-www-form-urlencoded' },
})
